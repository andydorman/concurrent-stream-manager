# concurrent-stream-manager
## API


* POST /start/{contentId} - Start the stream for the content with the given ID

  * Returns HTTP 200 with a GUID for the stream which can be used to send a heartbeat for the stream or stop the stream
  * Returns HTTP 403 when number of concurrent streams is exceeded



*  POST /heartbeat/{streamId} - Heartbeat to track playback still occurring

  * Returns HTTP 204 (regardless of whether the given stream ID exists)


* POST /stop/{streamId} - Stop the stream with the given ID

  * Returns HTTP 204 (regardless of whether the given stream ID exists)

## Running the service
```
npm i
npm start
```
The service will then be available at:

```
http://localhost:3000
```

This will redirect to swagger documentation where you can test the service.

Or if you prefer, which I think I propbably do, you can just use curl:

```
curl -iX POST "http://localhost:3000/user/andy.dorman" -H "accept: application/json"
```
will generate a JWT token which you can use in subsequent calls like this:
```
curl -iX POST "http://localhost:3000/start/terminator" -H "accept: application/json" -H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VySWQiOiJhbmR5LmRvcm1hbiJ9.x7NWGvfrI-x18Y6g8WK7qEwPrwkVW9tbByzUV_xbDeE"
```

Don't forget the `-i` flag to see the response headers in the case of failing calls.

You will need to visit the `user/:userId` endpoint to generate a JWT token which you can then use the authenticate against any of the otehr enspoints by entering it in the for `BEARER <TOKEN>` using any of the available swagger authentication windows, accessible by hitting one of the padlock icons. This only needs to be done once to automatically add the token to any subsequent calls.

The particular implmentation of swagger seems a little out of date, but it's the first time I've used it and the inline  documenation features won out this time - sorry!!

## Design decisions

The service is really in two parts:

* `/src/concurrentStreamManager` is where all of the logic lives. This is a simple class, exposed as a singleton in case it needed to be used from various different files around the service. In the end this wasn't actually necessary. I still like to seperate out the logic into something that could conceivably be easily swapped out at a later date, eaving the structure of the service itself largely intact.
* `/src/server.js` provide the configuration of the server itself, which in this case is a simple express.js server.

## Real world use

In the real world the service is designed to be used by intially registering a stream against a userId in the video player that is used to present the video. Heartbeat calls would then be made at regular intervals to keep the stream fresh. At the moment the service has a working streamLiftime of 30 seconds, actually 29999ms. Any stream older than is goes stale and is disregarded when new streams are added against a user. The functionality to prevent playback against stale streams would live elsewhere.

There is a stream limit of 3 per userId.

## If I didn't have toddling 3 year old identical twin boys and a 20 week old puppy in the house what I would also have done...

* I intended to markup the class that powers the service with JSDOC but ran out of time to do so
* Correctly configure a gitlab pipeline - I have the .gitlab-ci.yml in place to run this and deploy some documentation but obviosly it isn't cooperating
* I spent too long looking into better swagger implementations that would give a better experience when testing the app. These largely seemd to require much more configuration and I still wasn't convinvced I'd be able to get it to work in the time I had.
* I would have liked to return calls to add and refresh streams as promises to more closely represent how this may actually be used in the real world.

Hopefully what you can see here is enough to satisy the test, I'm finding it intensely difficult to complete any of these kind of technical tests given the two crazy little men we have running about at home at the moment!! I hope you can appreciate that!!
