const Stream = require('./stream');

class ConcurrentStreamManager {
  constructor() {
    this.streamLimit = 3;
    this.streamLifetime = 29999;
    this.streams = new Map();
  }

  isStale({ stream }) {
    return stream.updatedAt < Date.now() - this.streamLifetime;
  }

  removeStaleStreams({ userId }) {
    if (!this.streams.get(userId)) return;

    this.streams.get(userId).forEach(stream => {
      if (this.isStale({ stream })) {
        this.streams.get(userId).delete(stream.streamId);
      }
    });
  }

  addStream({ userId, contentId }) {
    try {
      this.removeStaleStreams({ userId });
      const userStreams = this.streams.get(userId) || new Map();
      if (userStreams.size >= this.streamLimit) {
        throw new Error(
          `You can only access ${this.streamLimit} streams concurrently`,
        );
      }

      const stream = new Stream({ userId, contentId });
      const { streamId } = stream;
      userStreams.set(streamId, stream);
      this.streams.set(userId, userStreams);

      return stream.streamId;
    } catch (e) {
      throw new Error('Unable to add stream', e);
    }
  }

  removeStream({ userId, streamId }) {
    try {
      this.streams.get(userId).delete(streamId);
      if (this.streams.get(userId).size === 0) this.streams.delete(userId);
    } catch (e) {
      throw new Error('Unable to remove stream', e);
    }
  }

  refreshStream({ userId, streamId }) {
    try {
      this.streams
        .get(userId)
        .get(streamId)
        .refresh();
    } catch (e) {
      throw new Error('This stream does not exist', e);
    }
  }

  resetStreams() {
    this.streams = new Map();
  }
}

module.exports = new ConcurrentStreamManager();
