const uuid = require('uuid/v4');

class Stream {
  constructor({ userId, contentId } = {}) {
    if (!userId || !contentId) {
      throw new Error('You must supply both a userId and a contentId');
    }
    const streamId = uuid();
    const now = Date.now();
    this.userId = userId;
    this.streamId = streamId;
    this.contentId = contentId;
    this.createdAt = now;
    this.updatedAt = now;
  }

  refresh() {
    this.updatedAt = Date.now();
  }
}

module.exports = Stream;
