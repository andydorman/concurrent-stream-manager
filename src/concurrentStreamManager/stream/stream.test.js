const MockDate = require('mockdate');

const Stream = require('./index');

describe('stream', () => {
  const now = Date.now();
  beforeEach(() => {
    MockDate.set(now);
  });

  afterEach(() => {
    MockDate.reset();
  });

  it('exports a function', () => {
    expect(typeof Stream).toEqual('function');
  });

  it('throws an error if no userId or contentId options are passed', () => {
    expect(() => new Stream()).toThrow();
  });

  describe('functionality:', () => {
    let stream;
    const contentId = 'myContent';
    const userId = 'myUser';

    beforeEach(() => {
      stream = new Stream({ contentId, userId });
    });

    it('creates new Stream setting the passed in userId and contentId as properties', () => {
      expect(stream.userId).toEqual(userId);
      expect(stream.contentId).toEqual(contentId);
    });

    it('initialises its createdAt and updatedAt properties to the current time', () => {
      expect(stream.createdAt).toEqual(now);
      expect(stream.updatedAt).toEqual(now);
    });

    it('will refresh the updatedAt property of a stream', () => {
      const then = Date.now();
      MockDate.set(then);
      stream.refresh();
      expect(stream.createdAt).toEqual(now);
      expect(stream.updatedAt).toEqual(then);
    });
  });
});
