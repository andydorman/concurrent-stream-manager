const MockDate = require('mockdate');
require('stream');

jest.mock('stream');

const ConcurrentStreamManager = require('./index');

describe('ConcurrentStreamManager', () => {
  it('exports an object', () => {
    expect(typeof ConcurrentStreamManager).toEqual('object');
  });

  it('initialises with an empty map of streams', () => {
    expect(ConcurrentStreamManager.streams.size).toEqual(0);
  });

  describe('functionality:', () => {
    const userId = 'foobar';
    const contentId = 'quxbaz';
    let streamId;

    beforeEach(() => {
      streamId = ConcurrentStreamManager.addStream({ userId, contentId });
    });

    afterEach(() => {
      ConcurrentStreamManager.resetStreams();
    });

    describe('addStream', () => {
      it('can add a stream for a given contentId against a given userId', () => {
        expect(ConcurrentStreamManager.streams.size).toEqual(1);
      });

      it('adds a stream for a known user against the appropriate stream key', () => {
        ConcurrentStreamManager.addStream({ userId, contentId: 'barfoo' });
        expect(ConcurrentStreamManager.streams.size).toEqual(1);
        expect(ConcurrentStreamManager.streams.get(userId).size).toEqual(2);
      });

      it('throws an error if any more than three streams are added for a given user', () => {
        ConcurrentStreamManager.addStream({ userId, contentId: 'barfoo' });
        ConcurrentStreamManager.addStream({ userId, contentId: 'bazqux' });
        expect(() =>
          ConcurrentStreamManager.addStream({ userId, contentId: 'bazqux' }),
        ).toThrow();
      });

      it('throws an error if either userId or contentId is not passed', () => {
        expect(() =>
          ConcurrentStreamManager.addStream({ userId: 'foo' }),
        ).toThrow();
        expect(() =>
          ConcurrentStreamManager.addStream({ contentId: 'foo' }),
        ).toThrow();
      });

      it('throws an error if no parameters are passed', () => {
        expect(() => ConcurrentStreamManager.addStream()).toThrow();
      });

      it('removes stale streams before attempting to add a new stream', () => {
        ConcurrentStreamManager.addStream({ userId, contentId: 'barfoo' });
        ConcurrentStreamManager.addStream({ userId, contentId: 'bazqux' });

        // Reduce the first stream's updatedAt value by 30 seconds
        const userStreams = ConcurrentStreamManager.streams.get(userId);
        const stream1 = userStreams.get(streamId);
        stream1.updatedAt -= 30000;
        userStreams.set(streamId, stream1);
        ConcurrentStreamManager.streams.set(userId, userStreams);

        expect(ConcurrentStreamManager.streams.get(userId).size).toEqual(3);
        expect(() =>
          ConcurrentStreamManager.addStream({ userId, contentId: 'bazqux' }),
        ).not.toThrow();
        expect(ConcurrentStreamManager.streams.get(userId).size).toEqual(3);
      });

      it('returns the streamId for a newly added stream', () => {
        expect(streamId).not.toBeNull();
      });
    });

    describe('refreshStream', () => {
      it('can refresh a given streamId for a given user', () => {
        const then = Date.now();
        MockDate.set(then);
        ConcurrentStreamManager.refreshStream({ userId, streamId });
        expect(
          ConcurrentStreamManager.streams.get(userId).get(streamId).updatedAt,
        ).toEqual(then);
      });

      it('throws and error if you try to refresh a non-existent user/stream combination', () => {
        expect(() =>
          ConcurrentStreamManager.refreshStream({ userId: 'quxbz', streamId }),
        ).toThrow();
      });

      it('throws an error if either userId or streamId is not passed', () => {
        expect(() =>
          ConcurrentStreamManager.refeshStream({ userId: 'foo' }),
        ).toThrow();
        expect(() =>
          ConcurrentStreamManager.refreshStream({ streamId: 'foo' }),
        ).toThrow();
      });

      it('throws an error if no parameters are passed', () => {
        expect(() => ConcurrentStreamManager.refeshStream()).toThrow();
      });
    });

    describe('removeStream', () => {
      it('can remove a stream by userId and streamId', () => {
        ConcurrentStreamManager.addStream({ userId, contentId: 'foobar' });
        ConcurrentStreamManager.removeStream({ userId, streamId });
        expect(ConcurrentStreamManager.streams.get(userId).size).toEqual(1);
        expect(ConcurrentStreamManager.streams.size).toEqual(1);
      });

      it('removes the userId key from the streams map if the last streamId is removed', () => {
        ConcurrentStreamManager.removeStream({ userId, streamId });
        expect(ConcurrentStreamManager.streams.has(userId)).toBeFalsy();
        expect(ConcurrentStreamManager.streams.size).toEqual(0);
      });

      it('throws an error if trying to remove a stream against a non-exstent user', () => {
        expect(() =>
          ConcurrentStreamManager.removeStream({ userId: 'quxbaz', streamId }),
        ).toThrow();
      });
    });
  });
});
