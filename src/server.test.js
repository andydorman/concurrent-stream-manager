const mockApp = {
  listen: jest.fn(),
  get: jest.fn(),
  post: jest.fn(),
};

const mockExpress = jest.fn().mockImplementation(() => mockApp);
jest.mock('express', () => mockExpress);

const mockExpressSwagger = jest.fn();
const mockExpressSwaggerGenerator = jest.fn(() => mockExpressSwagger);
jest.mock('express-swagger-generator', () => mockExpressSwaggerGenerator);

// Modules
const {
  startStream,
  stopStream,
  refreshStream,
} = require('./routes/streamManagement');
const encodeUserId = require('./routes/jwtToken');

const { app } = require('./server.js');

describe('Server', () => {
  it('should setup server with swagger', () => {
    expect(mockExpressSwaggerGenerator).toBeCalledWith(app);
    expect(mockExpressSwagger).toHaveBeenCalled();
    expect(mockExpress).toHaveBeenCalled();
  });

  it('should listen on port 3000', () => {
    expect(app.listen).toBeCalledWith(3000, undefined);
  });

  it('should bind POST /user/:userId to encodeUserId', () => {
    expect(app.post).toHaveBeenCalledWith('/user/:userId', encodeUserId);
  });

  it('should bind POST /heartbeat/:streamId to heartbeat', () => {
    expect(app.post).toHaveBeenCalledWith(
      '/heartbeat/:streamId',
      refreshStream,
    );
  });

  it('should bind POST /start/:contentId to startStream', () => {
    expect(app.post).toHaveBeenCalledWith('/start/:contentId', startStream);
  });

  it('should bind POST /start/:contentId to stopStream', () => {
    expect(app.post).toHaveBeenCalledWith('/stop/:contentId', stopStream);
  });
});
