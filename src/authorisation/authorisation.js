/** @module helper */
const jwt = require('jwt-simple');

const secret = 'secretStreamManagerKey';

const getToken = req => req.headers.authorization.split(' ')[1];

const decodeUserToken = token => jwt.decode(token, secret).userId;

const encodeUserToken = userId => jwt.encode({ userId }, secret);

module.exports = {
  decodeUserToken,
  encodeUserToken,
  getToken,
};
