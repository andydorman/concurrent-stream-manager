const express = require('express');

const app = express();
const expressSwagger = require('express-swagger-generator')(app);
const { version } = require('../package.json');
const {
  startStream,
  stopStream,
  refreshStream,
} = require('./routes/streamManagement');
const swagger = require('./routes/swagger');
const encodeUserId = require('./routes/jwtToken');

const serverSuccess = msg => console.log(msg);

const options = {
  swaggerDefinition: {
    info: {
      description:
        'A simple concurrent stream management  service.\n\
\n\
In order to correctly suthenticate against any of the endpoints you first need to retrieve a token using the `/user/{userId}` endpoint.\n\
You can then use this token in sn Authorization header in the form `Bearer <Token>` which you can input in any of the swagger authorisation popups.\n\
Otherwise you can simply use curl from the command line.',
      title: 'Concurrent Stream Management Service',
      version,
    },
    host: 'localhost:3000',
    basePath: '',
    produces: ['application/json'],
    schemes: ['http'],
    securityDefinitions: {
      JWT: {
        type: 'apiKey',
        in: 'header',
        name: 'Authorization',
        description:
          'JWT Bearer Token ("Bearer <Token>"). Get a token from the POST /user/:userId endpoint.',
      },
    },
  },
  basedir: __dirname,
  files: ['./routes/**/*.js'],
};

expressSwagger(options);

app.get('/', swagger);

app.post('/user/:userId', encodeUserId);

app.post('/start/:contentId', startStream);

app.post('/stop/:contentId', stopStream);

app.post('/heartbeat/:streamId', refreshStream);

app.listen(
  3000,
  serverSuccess('Concurrent Stream Server running on http://localhost:3000'),
);

module.exports = {
  app,
  serverSuccess,
};
