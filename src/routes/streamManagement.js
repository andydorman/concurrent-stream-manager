/** @module streamManagementRoutes */
const ConcurrentStreamManager = require('../concurrentStreamManager');
const { getToken, decodeUserToken } = require('../authorisation');
/**
 * @typedef Stream
 * @property {string} streamId - the unique ID for this stream
 */

/**
 * Start a stream for the given `contentId`.
 *
 * @route POST /start/{contentId}
 * @group Streams - Start and End streams
 * @param {string} contentId.path - ID of the content to stream
 * @returns {Stream} 200 - Json containing `streamId` for new stream
 * @returns {Error} 403 - When the user has already reached their maximum streams
 * @security JWT
 */

const startStream = (req, res) => {
  const userId = decodeUserToken(getToken(req));
  const { contentId } = req.params;

  try {
    const streamId = ConcurrentStreamManager.addStream({ userId, contentId });
    res.json({
      streamId,
    });
  } catch (error) {
    console.error(
      `Unable to add a ${contentId} stream for user ${userId} denied`,
      error,
    );
    res.status(403).end();
  }
};

/**
 * Stop the stream for the given `streamId`.
 *
 * @route POST /stop/{streamId}
 * @group Streams - Start and End streams
 * @param {string} streamId.path - ID of stream
 * @returns {void} 204
 * @security JWT
 */
const stopStream = (req, res) => {
  const userId = decodeUserToken(getToken(req));
  const { streamId } = req.params;

  ConcurrentStreamManager.removeStream({ userId, streamId });
  res.status(204).end();
};

/**
 * Perform a heartbeat for the stream with the given `streamId`.
 *
 * @route POST /heartbeat/{streamId}
 * @group Streams - Start and End streams
 * @param {string} streamId.path - ID of stream
 * @returns {void} 204
 * @security JWT
 */

const refreshStream = (req, res) => {
  const userId = decodeUserToken(getToken(req));
  const { streamId } = req.params;

  ConcurrentStreamManager.refreshStream({ userId, streamId });

  res.status(204).end();
};

module.exports = {
  startStream,
  stopStream,
  refreshStream,
};
