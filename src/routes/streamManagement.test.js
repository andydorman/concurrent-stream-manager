const { encodeUserToken } = require('../authorisation');

// Mocks
const mockConcurrentStreamManager = {
  addStream: jest.fn(),
  refreshStream: jest.fn(),
  removeStream: jest.fn(),
};
jest.mock('../concurrentStreamManager', () => mockConcurrentStreamManager);

const {
  startStream,
  stopStream,
  refreshStream,
} = require('./streamManagement');

const testUserId = 'testUserId';
const testToken = encodeUserToken(testUserId);
const testContentId = 'testContentId';
const testStreamId = 'testStreamId';

describe('streamManagement routes', () => {
  let req;

  beforeEach(() => {
    req = {
      headers: {
        authorization: `Bearer ${testToken}`,
      },
      params: {},
    };
  });

  describe('startStream', () => {
    it('returns streamId when ok', () => {
      mockConcurrentStreamManager.addStream.mockImplementationOnce(
        () => testStreamId,
      );
      const res = {
        json: jest.fn(),
      };

      req.params.contentId = testContentId;

      startStream(req, res);

      expect(res.json).toHaveBeenCalledWith({
        streamId: testStreamId,
      });
      expect(mockConcurrentStreamManager.addStream).toHaveBeenCalledWith({
        userId: testUserId,
        contentId: testContentId,
      });
    });

    it('returns 403 when stream errors', () => {
      // Suppress console.error, as it's expected
      console.error = jest.fn();

      mockConcurrentStreamManager.addStream.mockImplementationOnce(() => {
        throw new Error('Test');
      });
      const res = {
        status: jest.fn().mockReturnThis(),
        end: jest.fn().mockReturnThis(),
      };

      req.params.contentId = testContentId;

      startStream(req, res);

      expect(res.status).toHaveBeenCalledWith(403);
      expect(mockConcurrentStreamManager.addStream).toHaveBeenCalledWith({
        userId: testUserId,
        contentId: testContentId,
      });
    });
  });

  describe('refreshStream', () => {
    it('calls refreshStream on the ConcurrentStreamManager and returns a 204', () => {
      const res = {
        status: jest.fn().mockReturnThis(),
        end: jest.fn().mockReturnThis(),
      };

      req.params.streamId = testStreamId;

      refreshStream(req, res);

      expect(res.status).toHaveBeenCalledWith(204);
      expect(mockConcurrentStreamManager.refreshStream).toHaveBeenCalledWith({
        userId: testUserId,
        streamId: testStreamId,
      });
    });
  });

  describe('stopStream', () => {
    it('calls removeStream on the ConcurrentStreamManager and returns a 204', () => {
      const res = {
        status: jest.fn().mockReturnThis(),
        end: jest.fn().mockReturnThis(),
      };

      req.params.streamId = testStreamId;

      stopStream(req, res);

      expect(res.status).toHaveBeenCalledWith(204);
      expect(mockConcurrentStreamManager.removeStream).toHaveBeenCalledWith({
        userId: testUserId,
        streamId: testStreamId,
      });
    });
  });
});
