const { encodeUserToken } = require('../authorisation');

const encodeUserId = require('./jwtToken');

describe('encodeUserIdToken', () => {
  it('encodes a token which can then be decoded', () => {
    const testUserId = 'testUserId';
    const request = {
      params: {
        userId: testUserId,
      },
    };

    const response = {
      json: jest.fn(),
    };

    encodeUserId(request, response);

    expect(response.json).toHaveBeenCalledWith({
      userId: testUserId,
      token: encodeUserToken(testUserId),
    });
  });
});
