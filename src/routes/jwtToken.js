/** @module jwtToken */
const { encodeUserToken } = require('../authorisation');

/**
 * @typedef Token
 * @property {string} userId - the userId this token is for
 * @property {string} token - the JWT to use in the Authorization header (Authorization: Bearer <Token>)
 */

/**
 * Generate a valid JWT for the provided userId for use with the other endpoints.
 *
 * @route POST /user/{userId}
 * @group User - User operations
 * @param {string} userId.path - ID of the user to encode in the token.
 * @returns {Token} 200 - Json containing `streamId` for new stream
 * @returns {Error} 403 - When user cannot start a new stream
 * @security JWT
 */

const encodeUserId = (req, res) => {
  const { userId } = req.params;
  res.json({
    userId,
    token: encodeUserToken(userId),
  });
};

module.exports = encodeUserId;
