const swagger = require('./swagger');

describe('swagger redirect', () => {
  it('redirects to /api-docs', () => {
    const mockResponse = {
      redirect: jest.fn(),
    };
    swagger({}, mockResponse);

    expect(mockResponse.redirect).toHaveBeenCalledWith('/api-docs');
  });
});
